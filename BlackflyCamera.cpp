//==============================================================================
/*

*/
//==============================================================================

#include "BlackflyCamera.h"

//------------------------------------------------------------------------------
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

//------------------------------------------------------------------------------
using namespace std;
using namespace cv;
//------------------------------------------------------------------------------



BlackflyCamera::BlackflyCamera()
{
}

BlackflyCamera::~BlackflyCamera()
{
    FlyCapture2::Error error;
    error = camera.StopCapture();
    CheckError(error);

    error = camera.Disconnect();
    CheckError(error);

    cv::destroyAllWindows();
}

void BlackflyCamera::initialize(const unsigned int camera_index, bool start_capture)
{
    FlyCapture2::Error error;

    FlyCapture2::BusManager busMgr;
    FlyCapture2::PGRGuid guid;
    error = busMgr.GetCameraFromIndex(camera_index, &guid);
    CheckError(error);

    error = camera.Connect(&guid);
    CheckError(error);

    error = camera.GetCameraInfo(&camera_info);
    CheckError(error);

    //alternative is StartSynCapture for multicamera
    if (start_capture) {
        error = camera.StartCapture();
        CheckError(error);
    }

    window_name = "blackfly-"+std::to_string(getID());
    //cv::namedWindow(window_name, cv::WINDOW_AUTOSIZE);

    cv::namedWindow(window_name, cv::WINDOW_NORMAL);
    cv::setWindowProperty(window_name, cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);


    std::cout << "========================\n";
    std::cout << "exit             : esc\n";
    std::cout << "capture image    : space\n";
    std::cout << "========================\n";
}

void BlackflyCamera::startCapture()
{
    FlyCapture2::Error error;
    error = camera.StartCapture();
    CheckError(error);
}

unsigned int BlackflyCamera::getID()
{
    return camera_info.serialNumber;
}

void BlackflyCamera::setFrameRate(const unsigned int frame_rate)
{
    FlyCapture2::Error error;

    FlyCapture2::Property property;
    property.type = FlyCapture2::FRAME_RATE;
    error = camera.GetProperty( &property );
    CheckError(error);

    property.onOff = true;
    property.autoManualMode = false;
    property.absControl = true;
    property.absValue = frame_rate;
    error = camera.SetProperty(&property);
    CheckError(error);

    std::cout << "set frame rate" << std::endl;
}

unsigned int BlackflyCamera::maxPacketSize()
{
    FlyCapture2::Error error;

    unsigned int packet_size;
    error = camera.DiscoverGigEPacketSize(&packet_size);
    CheckError(error);

    return packet_size;
}

void BlackflyCamera::setPacket(const unsigned int packet_size, const unsigned int packet_delay)
{
    FlyCapture2::Error error;

    FlyCapture2::GigEProperty property;
    property.propType = FlyCapture2::PACKET_SIZE;
    error = camera.GetGigEProperty( &property );
    CheckError(error);

    property.value = packet_size;
    error = camera.SetGigEProperty(&property);
    CheckError(error);

    property = FlyCapture2::GigEProperty();
    property.propType = FlyCapture2::PACKET_DELAY;
    error = camera.GetGigEProperty( &property );
    CheckError(error);

    property.value = packet_delay;
    error = camera.SetGigEProperty(&property);
    CheckError(error);
}


void BlackflyCamera::setImage(const unsigned int width, const unsigned int height,
                              const unsigned int offset_w, const unsigned int offset_h)
{
    FlyCapture2::Error error;

    FlyCapture2::GigEImageSettings settings;
    error = camera.GetGigEImageSettings(&settings);
    CheckError(error);

    std::cout << "curr width    : " << settings.width << std::endl;
    std::cout << "curr height   : " << settings.height << std::endl;
    std::cout << "curr offset x : " << settings.offsetX << std::endl;
    std::cout << "curr offset y : " << settings.offsetY << std::endl;

    settings.width = width;
    settings.height = height;
    settings.offsetX = offset_w;
    settings.offsetY = offset_h;
    settings.pixelFormat = FlyCapture2::PIXEL_FORMAT_RAW8;
    //settings.pixelFormat = FlyCapture2::PIXEL_FORMAT_MONO8;
    //settings.pixelFormat = FlyCapture2::PIXEL_FORMAT_RGB8;

    error = camera.SetGigEImageSettings(&settings);
    CheckError(error);
}


void BlackflyCamera::setMode(FlyCapture2::Mode mode)
{
    FlyCapture2::Error error;
    error = camera.SetGigEImagingMode(mode);
    CheckError(error);
}


void BlackflyCamera::runVideoLoop()
{
    running = true;
    t_start = std::chrono::system_clock::now();
    while(isRunning()){
        captureImage();
        displayImage();
        ++frame_number;
    }
    t_end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = t_end-t_start;
    std::cout << "average fps: " << frame_number/elapsed_seconds.count() << "s\n";
}

void BlackflyCamera::captureImage()
{
     FlyCapture2::Error error;

    if (nonsync_multi_mode) {
        //slow multi-camera capture
        error = camera.StartCapture();
        CheckError(error);
    }

    error = camera.RetrieveBuffer( &rawImage );
    CheckError(error);

    if (nonsync_multi_mode) {
        //slow multi-camera capture
        error = camera.StopCapture();
        CheckError(error);
    }
}

void BlackflyCamera::displayImage()
{

    FlyCapture2::Error error;

    // Convert the raw image
    error = rawImage.Convert( FlyCapture2::PIXEL_FORMAT_RGB8, &convertedImage );
    CheckError(error);

    // Convert to opencv
    img = Mat(convertedImage.GetRows(), convertedImage.GetCols(), CV_8UC3,
              convertedImage.GetData());
    img.data = convertedImage.GetData();
    cv::cvtColor( img, img, CV_BGR2RGB );

    // Rectify image
    if (flag_rectify) {
         remap(img, rimg, rmap0, rmap1, CV_INTER_LINEAR);
    }


    // Display image
    if (flag_rectify){
        cv::imshow(window_name, rimg);
    } else {
        cv::imshow(window_name, img);
    }

    // Wait for key press forces image display
    char key = cv::waitKey(capture_delay);
    if (key!=-1){
        last_key = key;

        switch (key){
        case 32:
            saveImage();
            break;

        case 27:
            std::cout << "exiting" << std::endl;
            running = false;
        }
    }
}


void BlackflyCamera::saveImage(string file_name)
{
    if (file_name.empty()){
        system("mkdir -p ./pictures");
        capture_number++;
        file_name += "./pictures/frame_";
        file_name += std::to_string(capture_number);
        file_name += ".png";
        std::cout << "Image save to: " << file_name << std::endl;

        if (flag_rectify){
            cv::imshow(window_name, rimg);
        } else {
            cv::imshow(window_name, img);
        }
    }

    if (flag_rectify){
        cv::imwrite(file_name, rimg);
    } else {
        cv::imwrite(file_name, img);
    }
}

//==============================================
// STATIC FUNCTIONS
//==============================================

unsigned int BlackflyCamera::getNumCameras()
{
    FlyCapture2::Error error;
    FlyCapture2::BusManager busMgr;
    unsigned int numCameras = 0;
    error = busMgr.GetNumOfCameras(&numCameras);
    CheckError(error);

    return numCameras;
}

/*
void BlackflyCamera::syncMultiCamera( std::vector<BlackflyCamera*> remote_cameras )
{
    unsigned int num_cameras = remote_cameras.size();

    FlyCapture2::Camera** ppCameras = new FlyCapture2::Camera*[num_cameras];
    for (int i=0; i<num_cameras; ++i) {
        ppCameras[i] = &(remote_cameras[i]->camera);
        remote_cameras[i]->enableNonsyncMultiMode(); //not sure if this is necessary
    }

    FlyCapture2::Error error;
    error = FlyCapture2::Camera::StartSyncCapture( num_cameras, (const FlyCapture2::Camera**)ppCameras );
    CheckError(error);
}
*/



int BlackflyCamera::CheckError( FlyCapture2::Error error )
{
    if (error != FlyCapture2::PGRERROR_OK)
    {
        error.PrintErrorTrace();
        return -1;
    }
}
