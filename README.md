**Video player for the Point Grey Blackfly GiGE Camera**  

Authors : Brian Soe  
Contact : bsoe at stanford.edu

# Dependencies

- flycapture (point greys)
- opencv

# About

Uses Opencv to stream from the point grey blackfly.  

The code was developed for [Point Grey Blackfly GigE Camera].  
See [Blackfly Setup.md] for instructions on setting a persistent IP.


# Installation

### 0. Download the repository

```sh
cd Documents
git clone https://bitbucket.org/bsoe/blackfly-player.git
```

### 1. Download [opencv2] 

Download either using apt-get or from source.

#### Using apt-get

```sh
sudo apt-get install libopencv-dev
```

In the Cmakelists.txt, set the option for MANUALLY_LINK_OPENCV to OFF;

#### From source - [opencv repository]

```sh
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

git clone https://github.com/Itseez/opencv.git
cd ~/lib/opencv
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=~/lib/opencv-master/libopencv ..
make -j3
make install
```

In the Cmakelists.txt, set the option for MANUALLY_LINK_OPENCV to ON;

Make link to opencv in blackfly-player folder, if installing from source

```sh
cd Documents
git clone https://bsoe@bitbucket.org/bsoe/blackfly-player.git
cd blackfly-player
ln -s ~/lib/opencv-master/libopencv opencv
```

### 2. Download [Point Grey FlyCapture SDK]

Download requires a login. Registering is free. After download, unpack, and install

```sh
sudo apt-get install libraw1394-11 libgtkmm-2.4-1c2a libglademm-2.4-1c2a libgtkglextmm-x11-1.2-dev libgtkglextmm-x11-1.2 libusb-1.0-0
cd flycapture2-version
sh install_flycaputre.sh
```

### 3. Compile and run

```sh
cd Documents/blackfly-player/
mkdir build
cd build
cmake ..
make
./blackfly-player
```

[opencv2]:http://opencv.org
[opencv repository]:https://github.com/Itseez/opencv
[Point Grey Blackfly GigE Camera]:http://www.ptgrey.com/blackfly-gige-poe-cameras
[Point Grey FlyCapture SDK]:http://www.ptgrey.com/flycapture-sdk
[Blackfly Setup.md]: https://bitbucket.org/bsoe/blackfly-player/src/master/CAMERA_SETUP.md
