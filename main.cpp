#include "BlackflyCamera.h"

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

#include <flycapture/FlyCapture2.h>

#include <unistd.h>

int main(int, char**)
{
    std::cout << "number of cameras detected: "
              << BlackflyCamera::getNumCameras() << std::endl;

    BlackflyCamera camera;

    camera.initialize(0,false);

    // set image quality
    // bandwidth requirement = w*h*(frame rate)*24/(binning) MB
    camera.setFrameRate(10);
    int w = 640;                            // max = 1280/binning
    int h = 450;                            // max = 1024/binning
    camera.setImage(w,h,640-w,512-h);
    //camera.setMode(FlyCapture2::MODE_0);  // full resolution
    camera.setMode(FlyCapture2::MODE_1);    // 2x2 binning
    //camera.setMode(FlyCapture2::MODE_5);  // 4x4 binning
    
    // allocate bandwidth
    // smaller packet delay -> higher bandwidth
    // larger packet size   -> higher bandwidth
    // larger packet size   -> less cpu consumption
    //std::cout << "Largest allowed packet size: " << camera.maxPacketSize() << std::endl;
    camera.setPacket(600,6250);
    camera.startCapture();
    camera.runVideoLoop();

    return 0;
}
