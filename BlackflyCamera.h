//==============================================================================
/*

*/
//==============================================================================

//------------------------------------------------------------------------------
#include <flycapture/FlyCapture2.h>
#include <opencv2/core/core.hpp>

#include <string>
#include <iostream>
#include <chrono>

//------------------------------------------------------------------------------
//using namespace std;
//using namespace cv;
//------------------------------------------------------------------------------

class BlackflyCamera {

public:


    /*
    struct StereoRectifyMap {
        cv::Mat map0[2];
        cv::Mat map1[2];
    };
    */


    BlackflyCamera();

    ~BlackflyCamera();

    /** initialize the camera */
    void initialize(const unsigned int camera_index, bool start_capture=true);

    /** start image capture */
    void startCapture();

    /** get initialized cameras ID */
    unsigned int getID();

    /** set the video frame rate. set after initialization. */
    void setFrameRate(const unsigned int frame_rate);

    /** get largest packet size allowed by network */
    unsigned int maxPacketSize();

    /** set packet size */
    void setPacket(const unsigned int packet_size, const unsigned int packet_delay);

    /** set mode. pixels are sampled to reduce resolution. full resolution = 0 */
    void setMode(FlyCapture2::Mode mode);

    /** set image size */
    void setImage(const unsigned int width, const unsigned int height,
                  const unsigned int offset_w=0, const unsigned int offset_h=0);

    /** start the video loop. blocking. */
    void runVideoLoop();

    /** returns true if video loop is running */
    bool isRunning(){return running;}

    /** capture a single image */
    void captureImage();

    /** display the captured image */
    void displayImage();

    /** save picture to file. will automatically be named if empty */
    void saveImage(std::string file_name = "");

    /** captures a series of images and calculates the intrinsic and extrinsic parameters*/
    void calculateRectification(std::string& file_intrinsic,
                                std::string& file_extrinsic){
        std::cout << "rectification not implemented yet." << std::endl;
    }

    /** enable rectification. pass in path to camera parameter file (yaml). */
    void enableRectify(const std::string& file_intrinsic,
                       const std::string& file_extrinsic,
                       bool crop = true) {
        std::cout << "rectification not implemented yet." << std::endl;
    }

    /** enables synchronized capture, called automatically when syncMultiCamera used */
    void enableNonsyncMultiMode() {nonsync_multi_mode = true;}

    /** returns last key press */
    char lastKeyPressed(){return last_key;}

    /** set the last key back to default value of 0 */
    char clearLastKeyPressed(){last_key = 0;}

    unsigned int capture_delay = 6; //ms

    //=================
    // static functions
    //=================
    static unsigned int getNumCameras();

    //static void syncMultiCamera( std::vector<BlackflyCamera*> remote_cameras );

private:

    static int CheckError( FlyCapture2::Error error );

    //FlyCapture2::Camera camera;
    FlyCapture2::GigECamera camera;
    FlyCapture2::CameraInfo camera_info;

    FlyCapture2::Image rawImage;
    FlyCapture2::Image convertedImage;
    cv::Mat img;

    bool flag_rectify = false;
    cv::Mat rmap0;
    cv::Mat rmap1;
    cv::Mat rimg;

    std::string window_name;

    bool nonsync_multi_mode = false;

    char last_key = 0;
    bool running = false;
    unsigned long capture_number = 0;

    unsigned long long frame_number = 0;
    std::chrono::time_point<std::chrono::system_clock> t_start, t_end;
};



