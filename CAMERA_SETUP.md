***Blackfly GigE Camera Setup***
=============================

Table of Contents
-----------------

- Summary
- Documents
- Assigning Camera Static IP Address
- Assigning Computer Static IP Address
- Setting the Packet Size

Summary
-------
This document explains how to setup the Blackfly GigE Camera.

Documents
---------
[Website]  
[Getting Started]
[Datasheet] 

Assigning Camera Static IP Address
----------------------------------

This must be done on a Windows computer.

By default, the camera IP are set dynamically. We will setup a static IP address for the Blackfly.  

1. Plug in camera to the computer. Either use power cable, or POE power adaptor.
2. On a Windows computer, download and install [Flycapture-SDK], full installation.
3. Open the GigEConfigurator (Start->All Programs->Point Grey->Utilities->GigEConfigurator)
4. Click "Refresh". The Blackfly should show up in the left pane.
5. Select the camera. In the right pane, set the persistent IP  
Use the following settings, where X is large, but less than 255.  
```
IP: 192.168.1.X  
Submask: 255.255.0.0  
Gateway: Blank
```

Assigning Computer Static IP Address
------------------------------------

This should be done on the Ubuntu computer.


***Option 1***

1. Click the network icon is in the top right of the screen.  
2. Click *Edit Connections...*
3. Click *Add*
4. Select *Ethernet* and click *Create*
5. Change the connection name to *Camera*
6. In the *IPv4 Settings* tab, set method to *Manual*
7. Under *Addresses*, click *Add*
8. Set the IP address desired, where Y is neither 0, 255, nor X.  
   Address: 192.168.1.Y  
   Netmask: 255.255.0.0  
   Gateway: Blank or 0.0.0.0
   

***Option 2***

Set the computer ethernet card’s tcp4 ip address.  
Open up /etc/network/interfaces
```
sudo gedit /etc/network/interfaces
```
Add the following to the end of the file, using the IP address desired, where Y is neither 0, 255, nor X.  
```
auto eth0  
iface eth0 inet static  
address 192.168.1.Y  
netmask 255.255.0.0  
```
Now reset the ports 
```sh
sudo ifdown eth0
sudo ifup eth0
sudo /etc/init.d/networking restart
ifconfig
```
The new inet address should be: 192.168.168.Y.  
eth0 may periodically reset to the original ip until restart.


Setting the Packet Size
-----------------------
By default, packet size is set to 1400 bytes. The larger the packet size, the less processing overhead. To use jumbo packets up to size 9000, we must change the ethernet settings. These two options correspond to the same as the "assigning computer static ip address" section. Not all networks will support jumbo size.

***Option 1***

1. Click the network icon is in the top right of the screen.  
2. Click *Edit Connections...*
3. Select *Camera" and click edit
4. Change to the Ethernet tab
5. Set MTU to 9000

***Option 2***

Open up /etc/network/interfaces
```
sudo gedit /etc/network/interfaces
```
Add "mtu 9000" to the end of the eth0 that we created.
```
auto eth0  
iface eth0 inet static  
address 192.168.1.Y  
netmask 255.255.0.0  
mtu 9000  
```
Now reset the ports 
```sh
sudo ifdown eth0
sudo ifup eth0
sudo /etc/init.d/networking restart
```


[Website]: http://www.ptgrey.com/blackfly-gige-poe-cameras
[Datasheet]:http://www.ptgrey.com/support/downloads/10113/
[Getting Started]:http://www.ptgrey.com/support/downloads/10202
[Flycapture-SDK]: http://www.ptgrey.com/flycapture-sdk
